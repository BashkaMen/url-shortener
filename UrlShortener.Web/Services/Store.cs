﻿using MongoDB.Driver;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UrlShortener.Web.Core;

namespace UrlShortener.Web.Services
{
    public interface IStore
    {
        Task SaveAsync<T>(T item) where T : IEntity;
        Task<T> FindFirstOrDefaultAsync<T>(Expression<Func<T, bool>> expression) where T : IEntity;
        Task<bool> AnyAsync<T>(Expression<Func<T, bool>> expression) where T : IEntity;
    }

    public class Store : IStore
    {
        private readonly IMongoDatabase _database;

        public Store(IMongoDatabase database)
        {
            _database = database;
        }

        public Task SaveAsync<T>(T item) where T : IEntity
        {
            return GetCollection<T>().ReplaceOneAsync(s => s.Id == item.Id, item, new ReplaceOptions {IsUpsert = true});
        }

        public Task<T> FindFirstOrDefaultAsync<T>(Expression<Func<T, bool>> expression) where T : IEntity
        {
            return GetCollection<T>().Find(expression).FirstOrDefaultAsync();
        }

        public Task<bool> AnyAsync<T>(Expression<Func<T, bool>> expression) where T : IEntity
        {
            return GetCollection<T>().Find(expression).AnyAsync();
        }

        private IMongoCollection<T> GetCollection<T>() => _database.GetCollection<T>(typeof(T).Name);
    }
}