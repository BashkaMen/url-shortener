import axios from 'axios' 




let baseUrl = window.origin.indexOf('localhost') == -1 ? "" : "http://localhost:5002";
console.log(baseUrl);

export async function createUrl(url){
    let response = await axios.post(`${baseUrl}/api/url/create`, { Url: url });
    return response.data;
}
