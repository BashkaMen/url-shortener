using Amadevus.RecordGenerator;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using Westwind.AspNetCore.LiveReload;

namespace UrlShortener.Web
{
    [Record(Features.Default | Features.Equality)]
    public sealed partial class TestRecord
    {
        public Guid Id { get; }
        public string Name { get; }

        public int Age { get; }
    }

    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddCors();
            services.AddLiveReload(s => { s.FolderToMonitor = "wwwroot"; });

            services.AddUrlShortenerServices(s=>
            {
                _configuration.GetSection("Mongo").Bind(s);
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseLiveReload();
            }

            app.UseCors(s =>
            {
                s.AllowAnyOrigin();
                s.AllowAnyMethod();
                s.AllowAnyHeader();
            });


            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}