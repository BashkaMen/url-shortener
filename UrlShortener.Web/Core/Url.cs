﻿using System;
using Amadevus.RecordGenerator;

namespace UrlShortener.Web.Core
{
    [Record(Features.Default | Features.Equality)]
    public sealed partial class Url : IEntity
    {
        public Guid Id { get; }
        public string OriginalValue { get; }
        public string ShortKey { get; }
        public int ViewCount { get; }    
    }

    public static class UrlActions
    {
        public static Url Create(string url, string key) 
            => new Url(Guid.NewGuid(), url,  key, 0); 
            
        public static Url View(this Url url)
            => url.WithViewCount(url.ViewCount + 1);
        
    }
}