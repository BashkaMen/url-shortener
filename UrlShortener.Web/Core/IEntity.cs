﻿using System;

namespace UrlShortener.Web.Core
{
    public interface IEntity
    {
        public Guid Id { get; }
    }
}