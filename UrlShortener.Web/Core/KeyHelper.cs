﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UrlShortener.Web.Core
{
    public static class KeyHelper
    {
        private static readonly Random Random = new Random();

        public static string GenerateKey(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            var key = Enumerable.Range(0, length)
                .Aggregate(new StringBuilder(), 
                    (curr, next) => curr.Append(chars.GetRandom()));

            return key.ToString();
        }

        public static char GetRandom(this IEnumerable<char> source)
        {
            var s = source.ToString();

            return s[Random.Next(s.Length)];
        }
    }
}