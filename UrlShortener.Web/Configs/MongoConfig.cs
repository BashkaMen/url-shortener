﻿namespace UrlShortener.Web.Configs
{
    public class MongoConfig
    {
        public string Connection { get; set; }
        public string DataBaseName { get; set; }
    }
}
