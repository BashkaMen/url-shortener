﻿using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using UrlShortener.Web.Configs;
using UrlShortener.Web.Services;

namespace UrlShortener.Web
{
    public static class Configure
    {
        public static void AddUrlShortenerServices(this IServiceCollection services, Action<MongoConfig> mongoConfig)
        {
            services.Configure(mongoConfig);

            services.AddSingleton(s=>
            {
                var config = s.GetRequiredService<IOptions<MongoConfig>>().Value;
                var url = new MongoUrl(config.Connection);
                var client = new MongoClient(url);

                return client.GetDatabase(config.DataBaseName);
            });

            services.AddTransient<IStore, Store>();
        }
    }
}
