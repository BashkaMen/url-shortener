﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UrlShortener.Web.Core;
using UrlShortener.Web.RequestModel;
using UrlShortener.Web.Services;

namespace UrlShortener.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UrlController : ControllerBase
    {
        private readonly IStore _store;

        public UrlController(IStore store)
        {
            _store = store;
        }


        [HttpPost("create")]
        public async Task<Url> Create(CreateUrlRequest request)
        {
            var key = await CreateUniqKey();
            var url = UrlActions.Create(request.Url, key);

            await _store.SaveAsync(url);

            return url;
        }


        [HttpGet("/{key}")]
        public async Task<IActionResult> Execute(string key)
        {
            var url = await _store.FindFirstOrDefaultAsync<Url>(s => s.ShortKey == key);
            if (url == null)
                return NotFound("Short url not found");

            var viewedUrl = url.View();
            await _store.SaveAsync(viewedUrl);

            return new RedirectResult(url.OriginalValue);
        }


        private async Task<string> CreateUniqKey()
        {
            while (true)
            {
                var key = KeyHelper.GenerateKey(8);
                if (!await _store.AnyAsync<Url>(s => s.ShortKey == key))
                    return key;
            }
        }
    }
}