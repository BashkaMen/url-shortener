﻿using System.ComponentModel.DataAnnotations;

namespace UrlShortener.Web.RequestModel
{
    public class CreateUrlRequest
    {
        [Required]
        [Url]
        public string Url { get; set; }
    }
}
